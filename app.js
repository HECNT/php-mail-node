var express = require("express");
var app = express();
var ctrl = require('./controller/ctrl')

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get("/enviar-email/:usuario", setMail);
app.get("/", inicio);



function inicio(req, res) {
  res.send('Hola')
}

function setMail(req, res) {
  var usuario = req.params.usuario
  ctrl.getUsuario(usuario)
  .then(function(result){
	if (result.err) {
		res.json({err: true, description:"No se encontro el usuario-----"})
	} else {
		console.log(usuario,'***************');
    		ctrl.setUpdate(usuario, result)
    		.then(function(result){
     			 console.log(result);
      			res.json({err: false})
   		 })
	}
    
  })
}


app.listen(3051, function() {
  console.log('Escuchando en el puerto del 3051');
})
