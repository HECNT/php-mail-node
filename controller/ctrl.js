

var mysql      = require('mysql');
var nodemailer = require('nodemailer');
var connection = require('../models/main');
var md5        = require('md5');

function getPassMd5() {
  var pass = Math.random().toString(36).substring(9) + Math.random().toString(36).substring(9);
  var arr = [md5(pass), pass];
  return arr;
}

module.exports.getUsuario = function (usuario) {
  return new Promise(function(resolve, reject) {
    connection.query('SELECT * FROM usuarios where cliente_numero = ?', [usuario] ,function (error, results, fields) {
      if (error) {
        console.log(error,'****************************')
	       resolve({err:true, description:"No se encontro el usuario"});
    	} else {
    		if (results.length == 0) {
    			resolve({err:true, description:"No se encontro el usuario"});
    		} else {
          var email = results[0].cliente_email;
    			resolve({err:false,email:email})
    		}
    	}
    });
  });
}

module.exports.setUpdate = function (usuario, d) {
  console.log(d,'***************************')
  return new Promise(function(resolve, reject) {
    var pass = getPassMd5()
    connection.query('UPDATE usuarios SET cliente_clave = ? where cliente_numero = ?',[pass[0],usuario] ,function (error, results, fields) {
      if (error) {
        console.log(error);
        console.log('Hubo un error al actualizar la tabla');
        resolve({err: true, description: error})
      } else {
        var email = d.email;
            var transporter = nodemailer.createTransport({
             service: 'gmail',
             auth: {
                user: 'jhernandez.makicop@gmail.com',
               pass: 'lemaos1967'
              }
           });

      const mailOptions = {
        from: 'jhernandez.makicop@gmail.com',
        to: email,
        subject: 'Password reseteo',
        html: '<h1>Hola '+ usuario +', Se reinicio tu password a '+pass[1]+'</h1>'
      };

      transporter.sendMail(mailOptions, function (err, info) {
        if(err) {
          console.log(err, 'Hubo error')
          resolve({err:true, description : "Hubo un error al enviar el correo"})
        } else {
          console.log(info, 'Se envio correo');
          resolve({err: false, description: 'Se actualizo'})
        }
      });
      }
    });
  });
}
